#!/bin/bash

: << __docstring__

Nombre: procesos.sh
Autor: Marco Cuauhtli Elizalde Sanchez

    Lista los procesos activos de un servidor con un SO tipo Unix
    y los convierte en un formato html o json amigables, y guarda esta informacion
    en un archivo, en la misma carpeta, llamado por defecto "reporte.<la extension>"

Dependencias:

    - process_to_format
    - python (2 o 3)

Parametros:

    Acepta ninguno o alguno de los siguientes patrones:

        -(r|-rango)=[[:digit:]]*
        ^-(b|-byte)$
        ^-(k|-kilobyte)$
        ^-(mb|-megabyte)$
        --unit=.*
        ^--(html|xml|json|md)$

Notas:

    Antes de intentar correrse, es buena idea cerciorase de que los archivos
    procesos.sh y process_to_format tengan permisos de ejecucion.
    Ejemplo: chmod +x procesos.sh process_to_format

Ejemplos:
    
    Correr dentro de la misma carpeta donde se encuentre el script:

    ./procesos.sh --rango=20 --megabyte --html
    ./procesos.sh --rango=20 --byte --html
    ./procesos.sh --rango=50 --unit=b --json

__docstring__

#[START] Config
declare rango=10     # Valores por defecto
declare unidad=mb    # Valores por defecto
declare format=html  # Valores por defecto

declare _date="$(date +'%d/%m/%Y')"
declare _time="$(date +'%H:%M%p')"

declare output_file="./report"  #  Cambiar nombre a gusto.
                                #+ Al final se guardara en un archivo con la extension
                                #+ correspondiente, por lo que solo se debe de cambiar el
                                #+ nombre y no anadir una extension.
#[END] Config

#[START] Funciones
get_opts() {
	while [[ $# > 0 ]]
	do
        #[START] Obtener rango        
		if [[ "$1" =~ -(r|-rango)=[[:digit:]]* ]]
        then
            export rango=${1##*=}
            shift
        #[END] Obtener rango        

        #[START] Obtener unidades        
        elif [[ "$1" =~ ^-(b|-byte)$ ]]
        then
            export unidad=b
            shift
        elif [[ "$1" =~ ^-(k|-kilobyte)$ ]]
        then
            export unidad=kb
            shift
        elif [[ "$1" =~ ^-(mb|-megabyte)$ ]]
        then
            export unidad=mb
            shift
        elif [[ "$1" =~ --unit=.* ]]
        then
            export unidad=${1##*=}
            shift
        #[END] Obtener unidades      

        #[START] Obtener formato
        elif [[ "$1" =~ ^--(html|xml|json|md)$ ]]
        then
            export format=${1##*-}
            shift
        #[END] Obtener formato

        else
            shift
		fi
	done
}

pcr_processes() { ps -eo{pid,comm,rss}=; }

convert_memory_size() {
    read -rd $'\0' _pcr_processes

    [[ "$1" = "-u" ]] && shift && declare _unidad="$1"

    if [[ "$_unidad" = "b" ]]
    then
        awk -F "," -v OFS="," '{$3=int($3*1000);print}'<<< "$_pcr_processes"
    elif [[ "$_unidad" = "kb" ]]
    then
        echo "$_pcr_processes"
    elif [[ "$_unidad" = "mb" ]]
    then
        awk -F "," -v OFS="," '{$3=int($3/1024);print}'<<< "$_pcr_processes"
    fi
}

#[END] Funciones

#[START] Main
get_opts $@

pcr_processes \
    | awk '{$1=$1}1' OFS="," \
    | convert_memory_size -u "$unidad" \
    | python ./process_to_format \
        --fecha "$_date" \
        --hora "$_time" \
        --rango "$rango" \
        --unidad "$unidad" \
        --format "$format" > "$output_file.${format}"
#[END] Main