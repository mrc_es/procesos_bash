# 1. Descripción 

Lista los procesos activos de un servidor con un SO tipo Unix y, con ayuda de scripts en python, los convierte en un formato html o json amigables guardando esta información en un archivo —en la misma carpeta— llamado por defecto "reporte.\<la extension deseada\>"

## 1.1 Contenido Y Breve Descripción Del Contenido.

El repositorio contiene lo siguiente: 

* *procesos.sh* - Script principal. Ahí se encuentra al inicio, en la documentación, como cambiar permisos para ejecutar, algunos ejemplos, y se listan algunas dependencias como tener python instalado.
* *process_to_format* - El archivo python que necesita para generar el reporte tanto en html como en json.
* templates.py - Cadenas de python que usará el archivo *process_to_format* para construir el reporte. Las cadenas contienen código html con el diseño de la tabla.

# 2. Cómo Ejecutar

Antes de intentar ejecutar, es buena idea cerciorase de que los archivos
*procesos.sh* y *process_to_format* tengan permisos de ejecución.

Ejemplo: 

```bash
$ chmod +x procesos.sh process_to_format
```
## 2.1 Parametros Aceptados.

Utiliza ninguno o alguno de los siguientes patrones como parámetros:

* -(r|-rango)=[[:digit:]]*
* ^-(b|-byte)$
* ^-(k|-kilobyte)$
* ^-(mb|-megabyte)$
* --unit=.*
* ^--(html|xml|json|md)$

## 2.2 Ejemplos de Ejecución.
    
Ejecutar dentro de la misma carpeta donde se encuentre el script.

Algunos ejemplos con los que funciona el script, son:

```bash
$ ./procesos.sh --rango=20 --megabyte --html
$ ./procesos.sh --rango=20 --byte --html
$ ./procesos.sh --rango=50 --unit=b --json
$ ./procesos.sh
```

# 3. Información Sobre Dependencias

Se necesitar tener un interprete de Python instalado. Actualmente se puede ejecutar con las versiones 2.7.12 y 3.6.9

Actualmente se requiere:

* dicttoxml

En el caso extraño de no tenerse, se pueden instalar con `pip install <dependencia>`. Incluso se podía hacer un script que verifique las dependencias y/o las instale.


# 4. Ejemplos de Salida

El archivo reporte.json contendría algo por el estilo:

```json
{
  "fecha": "28/08/2020",
  "hora": "19:55PM",
  "rangos": [
    {
      "rango": "1-20",
      "memoria": "300.0KB",
      "procesos": [
        {
          "pid": "1",
          "nombre": "init",
          "memoria": "300KB"
        }
      ]
    },
    {
      "rango": "11741-11760",
      "memoria": "9824.0KB",
      "procesos": [
        {
          "pid": "11756",
          "nombre": "init",
          "memoria": "228KB"
        },
        {
          "pid": "11757",
          "nombre": "zsh",
          "memoria": "9596KB"
        }
      ]
    }
  ]
}
```

El archivo reporte.html contendría algo por el estilo:

```html
<html>
</br>
<b>Fecha</b>: 28/08/2020
</br>
<b>Hora</b>: 20:15PM
</br>
</br>
Reporte:
</br>

</br>
<b>rango</b>: 1-10
</br>
<b>memoria</b>: 0.0MB
</br>
<b>procesos</b>: 
</br>

<table border="1px">
<tr>
    <td>nombre</td>
    <td>init</td>
</tr>
<tr>
    <td>pid</td>
    <td>1</td>
</tr>
<tr>
    <td>memoria</td>
    <td>0MB</td>
</tr>
</table>
</br>

</br>

</html>
```